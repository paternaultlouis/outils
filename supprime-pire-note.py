#!/usr/bin/env python
"""Affiche la moyenne de chaque élève, en supprimant la moins bonne note si l'élève a été présent à toutes les évaluations.

- Copier le tableau de notes de Pronote
- Supprimer les colonnes non pertinentes (moyenne, classe, et notes à ne pas prendre en compte ici)
- Ne pas toucher aux colonnes
- Enregistrer au format .csv
- Lancer ce programme dessus
"""

import csv
import decimal
import sys


def moyenne(tuples):
    valeur = decimal.Decimal(0)
    total = decimal.Decimal(0)

    for i in tuples:
        if i is None:
            continue
        valeur += i[0]
        total += i[1]

    if total == 0:
        return None
    return valeur * 20 / total


def petite(tuples):
    ipire = 0
    vpire = float("inf")

    for i, item in enumerate(tuples):
        if item[0] / item[1] < vpire:
            vpire = item[0] / item[1]
            ipire = i

    return ipire


with open(sys.argv[1]) as csvfile:
    dates, totaux, *élèves = list(csv.reader(csvfile, delimiter=","))

    # On extrait le nombre de points de chaque devoirs, présenté sous la forme :
    # - "COEF-/TOTAL" si le total n'est pas sur 20 ;
    # - "COEF" si le total est sur 20.
    totaux = [0] + [
        int(cell.split("/")[-1]) if "/" in cell else 20 for cell in totaux[1:]
    ]

    # Lit et interprète le tableau
    for y in range(len(élèves)):
        for x in range(len(élèves[y])):
            try:
                élèves[y][x] = decimal.Decimal(élèves[y][x].replace(",", "."))
            except decimal.InvalidOperation:
                pass

    # Supprime la plus petite note
    for ligne in élèves:
        notes = [
            ((ligne[i], totaux[i]) if isinstance(ligne[i], decimal.Decimal) else None)
            for i in range(1, len(ligne))
        ]
        if notes.count(None) == 0:
            # On supprime la moins bonne
            indice = petite(notes)
            print(
                ligne[0], moyenne([notes[i] for i in range(len(notes)) if i != indice])
            )
        else:
            print(ligne[0], moyenne(notes))
