#!/usr/bin/env python

# Copyright 2023 Louis Paternault
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this.  If not, see <http://www.gnu.org/licenses/>.

import textwrap

from IPython.display import HTML

TABLEAU_TETE = """
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
  </head>
  <body>
                <table style="border-collapse: collapse;">
<thead>
    <tr style="border-bottom:2px solid black">
        <th>Appel</th><th>Résultats<br>attendus</th><th>Résultats<br>obtenus</th><th></th>
    </tr>
</thead>
<tbody>
"""
TABLEAU_LIGNE_OK = textwrap.dedent(
    """\
        <tr style="border-bottom:1px solid black">
            <td><code>{fonction}({arguments})</code></td>
            <td><code>{attendu}</code></td>
            <td><code>{obtenu}</code></td>
            <td>&#x2705;</td>
        </tr>
        """
)
TABLEAU_LIGNE_ERREUR = textwrap.dedent(
    """\
        <tr style="border-bottom:1px solid black">
            <td><code>{fonction}({arguments})</code></td>
            <td><code>{attendu}</code></td>
            <td><code>{obtenu}</code></td>
            <td>&#x274C;</td>
        </tr>
        """
)
TABLEAU_LIGNE_EXCEPTION = textwrap.dedent(
    """\
        <tr style="border-bottom:1px solid black">
            <td><code>{fonction}({arguments})</code></td>
            <td><code>{attendu}</code></td>
            <td><em>Erreur : <code>{obtenu}</code></em></td>
            <td>&#x274C;</td>
        </tr>
        """
)
TABLEAU_PIED = """</tbody></table>
</body></html>"""


def check(fonction, fixtures, *, traitement=None):
    if traitement is None:
        traitement = lambda x: x
    text = TABLEAU_TETE
    for args, attendu in fixtures:
        try:
            obtenu = fonction(*args)
            if traitement(obtenu) == traitement(attendu):
                text += TABLEAU_LIGNE_OK.format(
                    fonction=fonction.__name__,
                    arguments=", ".join(map(str, args)),
                    attendu=attendu,
                    obtenu=obtenu,
                )
            else:
                text += TABLEAU_LIGNE_ERREUR.format(
                    fonction=fonction.__name__,
                    arguments=", ".join(map(str, args)),
                    attendu=attendu,
                    obtenu=obtenu,
                )
        except Exception as erreur:
            text += TABLEAU_LIGNE_EXCEPTION.format(
                fonction=fonction.__name__,
                arguments=", ".join(map(str, args)),
                attendu=attendu,
                obtenu=str(erreur),
            )

    return HTML(text + TABLEAU_PIED)

if __name__ == "__main__":

    def tteesstt(a, b):
        return abs(a / b)

    print(
        check(
            tteesstt,
            (
                ((1, 2), 0.5),
                ((3, 4), 0.75),
                ((3, 0), 0),
                ((-1, 1), -1),
            ),
        )
    )
