# Outils

Divers outils pas nécessairement spécifiques aux mathématiques, en tout cas pas spécifiques à un niveau donné.

Ces fichiers sont publiées sous les licences suivantes, au choix :

* [LaTeX Project Public License](http://latex-project.org/lppl/), version 1.3c ou supérieure ;
* [Do What The Fuck You Want To Public License](http://www.wtfpl.net/) ou sa traduction française la [Licence Publique Rien À Branler](http://sam.zoy.org/lprab/) ;
* [CC0 1.0 universel](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) ;
* versé dans le domaine public, dans les législations qui le permettent (ce qui n'est pas le cas de la France).
