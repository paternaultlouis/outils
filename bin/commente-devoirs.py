#!/usr/bin/env python3

# Copyright 2022-2023 Louis Paternault
#
# Publié sous les licences suivantes :
#
# - [Do What The Fuck You Want To Public License](http://www.wtfpl.net/) et sa traduction française la [Licence Publique Rien À Branler](http://sam.zoy.org/lprab/) ;
# - [CC0 1.0 universel](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) ;
# - versé dans le domaine public, dans les législations qui le permettent (ce qui n'est pas le cas de la France).

"""Commente les fichiers au format .tex du répertoire courant.

Ce programme ne prend aucun argument.

- Le commentaire est un fichier .md.
- Dans le cas de fichiers toto-a.tex, toto-b.tex, toto-corrige.tex, toto-blanc.tex etc., ne commente que le premier.
- Utilise la variable d'environnement EDITOR pour modifier le fichier.
- Utilise la variable d'environnement PDFVIEWER pour afficher le PDF (ou `gio open` par défaut).
- Les fichiers sont supposés compilés (un fichier pdf existe pour chaque fichier tex).
"""

import logging
import pathlib
import re
import contextlib
import subprocess
import shutil
import sys
import tempfile
import os

RE_IGNORÉ = re.compile(r".*-([b-z]|blanc|corrige|bis|ter).tex")
PDF_VIDE = """%PDF-1.0
1 0 obj<</Type/Catalog/Pages 2 0 R>>endobj 2 0 obj<</Type/Pages/Kids[3 0 R]/Count 1>>endobj 3 0 obj<</Type/Page/MediaBox[0 0 3 3]>>endobj
trailer<</Size 4/Root 1 0 R>>
"""  # Merci https://stackoverflow.com/a/17280876


def fichiers():
    for nom in sorted(pathlib.Path(".").glob("*.tex")):
        if RE_IGNORÉ.match(str(nom)):
            logging.info(f"Fichier {nom} ignoré.")
            continue
        yield nom

def pdfviewer():
    viewer = os.environ.get("PDFVIEWER")
    if viewer:
        return [viewer]
    return ["gio", "open"]

@contextlib.contextmanager
def affichePDF():
    with tempfile.TemporaryDirectory() as tempdir:
        pdfname = pathlib.Path(tempdir) / "temp.pdf"
        with open(pdfname, mode="w") as pdffile:
            pdffile.write(PDF_VIDE)
           
        process = subprocess.Popen(
            pdfviewer() + [pdfname],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )

        yield pdfname

        logging.info("Fermeture du lecteur PDF.")
        process.kill()


def main():
    if len(sys.argv) != 1:
        print(__doc__.strip())
        sys.exit(1)

    with affichePDF() as pdfname:
        for fichiertex in fichiers():
            shutil.copy(fichiertex.with_suffix(".pdf"), pdfname)
            subprocess.run(
                [os.environ.get("EDITOR", "vim"), f"{fichiertex}.md"], check=True
            )


if __name__ == "__main__":
    main()
